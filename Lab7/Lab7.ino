#include <SPI.h>
#include <MFRC522.h>

MFRC522 mfrc522(2, 0);
uint64_t uidDec, uidDecTemp;
int state;
uint64_t time;
void setup() {
  Serial.begin(9600);
  while (!Serial);
  SPI.begin();
  pinMode(LED_BUILTIN,OUTPUT);
  
  mfrc522.PCD_Init();
  mfrc522.PCD_DumpVersionToSerial();
  Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
}
int timerCount(int timeOut){
  if((millis()-time)==timeOut) return 1;
  else {return 0;}  
}
void resetTimer(){
  time=millis();
}
void loop() {
  switch(state){
    case 0:
      digitalWrite(LED_BUILTIN,HIGH);
      if (!mfrc522.PICC_IsNewCardPresent())
          state=0;
      else
          state=1;
      break;
    case 1:
      if ( ! mfrc522.PICC_ReadCardSerial())
        state=0;
      else
        state=2;
      break;
    case 2:
      uidDec = 0;
      for (byte i = 0; i < mfrc522.uid.size; i++) {
        uidDecTemp = mfrc522.uid.uidByte[i];
        uidDec = uidDec*256+uidDecTemp;
      }
      Serial.println(uidDec);
      resetTimer();
      state=3;
      break;
    case 3:
      if(uidDec == 8312548355){
        digitalWrite(LED_BUILTIN,LOW);      
        if(timerCount(3000)==1) {state=0; resetTimer();}
        break;
      }
  }
}
